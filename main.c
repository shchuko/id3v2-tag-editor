// Test programs
#include <stdio.h>
#include <string.h>

#include "tag_id3.h"

void printArgvError() {
    printf("Wrong arguments!\n");
}

void helpInfo() {
    printf("\nSupported commands:\n");
    printf("\t--filepath=<path>\tPath to file will be opened. This argument will be entered the first\n");
    printf("\t--show\t\t\tShow file's tag data\n");
    printf("\t--frames-list\t\tShow supported frames IDs and descriptions\n");
    printf("\t--get=<frame id>\tPrint frame information from tag\n");
    printf("\t--set=<frame id>\tChoose frame to edit. Next argument will be --value=\n");
    printf("\t--value=<data>\t\tWrite data to chosen frame. Previous command will be --set=\n");
    printf("\t--help\t\t\tShow help info\n");
}

int main(int argc, char* argv[]) {
    const char key_filepath[] = "--filepath=";
    int key_filepath_len = strlen(key_filepath);

    const char key_show[] = "--show";
    int key_show_len = strlen(key_show);

    const char key_set[] = "--set=";
    int key_set_len = strlen(key_set);

    const char key_get[] = "--get=";
    int key_get_len = strlen(key_get);

    const char key_value[] = "--value=";
    int key_value_len = strlen(key_value);

    const char key_help[] = "--help";
    int key_help_len = strlen(key_help);

    const char key_frames[] = "--frames-list";
    int key_frames_len = strlen(key_frames);

    if (argc < 2) {
        printArgvError();
        helpInfo();
        return 0;
    }

    char filepath[255];
    if ((bool) strncmp(argv[1], key_filepath, (size_t) key_filepath_len)) {
        printArgvError();
        helpInfo();
        return 0;
    }

    for (int i = key_filepath_len; i < strlen(argv[1]) + 1; ++i)
        filepath[i - key_filepath_len] = argv[1][i];

    ID3Tag tag;
    unsigned char *buffer = NULL;
    long buffer_size = 0;

    if (!id3TagSave(filepath, &tag, &buffer, &buffer_size)) {
        printf("Reading file error\n");
        return 0;
    }

    if (tag.v2_tag_begin != -1 && getTagVersion(&tag) != 3 && getTagVersion(&tag) != 4) {
        printf("Unsupported tag version\n");
        return 0;
    }

    if (tag.v2_tag_begin == -1 && tag.v1_tag_begin == -1) {
        printf("No ID3 tag inside\n");
        return 0;
    }
    else {
        printf("File opening successful\n");
        printf("ID3v1 tag: ");
        if (tag.v1_tag_begin == -1)
            printf("NOT FOUND\n");
        else
            printf("READING SUCCESSFUL\n");

        printf("ID3v2");
        if (tag.v2_tag_begin == -1)
            printf(" tag: NOT FOUND\n");
        else
            printf(".%i tag: READING SUCCESSFUL\n", getTagVersion(&tag));
    }

    long old_v1tag_begin_pos = tag.v1_tag_begin;
    long old_v2tag_begin_pos = tag.v2_tag_begin;
    long olg_v2tag_full_size = tag.v2_tag.tag_size;
    if (tag.v2_tag.header.extended_flag) {
        olg_v2tag_full_size += 10;
        if (tag.v2_tag.header.crc_flag)
            olg_v2tag_full_size += 10;
    }

    bool file_need_to_write = false;

    for (int i = 2; i < argc; ++i) {
        static bool waiting_for_value = false;
        static unsigned char set_frame_id[255];
        char data[255];

        // If previous command was --set=
        if (waiting_for_value) {
            waiting_for_value = false;

            // If the command id --value
            if (!strncmp(argv[i], key_value, (size_t) key_value_len)) {
                for (int j = key_value_len; j < strlen(argv[i]) + 1; ++j)
                    data[j - key_value_len] = argv[i][j];

                if (tag.v2_tag_begin == -1) {
                    printf("\nThere was no v2 tag, trying to create it from v1 tag...");
                    if (createID3v2Tag(&tag)) {
                        printf(" SUCCESS\n");
                    } else
                        printf(" FAIL\n");
                }

                switch (setTextFrameData(set_frame_id, &tag, (unsigned char *) data, (long) strlen(data), false)) {
                    case 3:
                        printf("\nFrame '%s' writing to v1 tag and v2 tag SUCCESSFUL\n", set_frame_id);
                        file_need_to_write = true;
                        break;

                    case 2:
                        printf("\nFrame '%s' writing to v2 tag SUCCESSFUL, to v1 tag FAILED\n", set_frame_id);
                        file_need_to_write = true;
                        break;

                    case 1:
                        printf("\nFrame '%s' writing to v1 tag SUCCESSFUL, to v2 tag FAILED\n", set_frame_id);
                        file_need_to_write = true;
                        break;

                    case 0:
                        printf("\nFrame '%s' writing FAILED\n", set_frame_id);
                        break;

                    default:
                        break;
                }
                continue;
            } else {
                printf("\nFew arguments for '%s', expected '%s' after '%s'\n", key_set, key_value, key_set);
            }
        }

        // If command is --frames-list
        if (!strncmp(argv[i], key_frames, (size_t) key_frames_len)) {
            printSupportedFrames(&tag, stdout);
        }
            // If command is --show
        else if (!strncmp(argv[i], key_show, (size_t) key_show_len)) {
            printf("\nAll not-clear frames found in file:\n");
            printAllTextFramesData(stdout, &tag, false);
        }
            // If command is --get
        else if (!strncmp(argv[i], key_get, (size_t) key_get_len)) {
            for (int j = key_get_len; j < strlen(argv[i]) + 1; ++j)
                data[j - key_get_len] = argv[i][j];

            if ((tag.v2_tag_begin != -1 && isFrameNameExists((unsigned char *) data, getTagVersion(&tag))) ||
                (tag.v1_tag_begin != -1 && isFrameNameExists((unsigned char *) data, -1))) {
                printf("\n");
                printTextFrame((unsigned char *) data, stdout, &tag, true);
            } else {
                printf("\nFrame '%s' if not supported\n", data);
                printf("Call %s to view supported frames for this file\n", key_frames);
            }
        }
            // If command is --set
        else if (!strncmp(argv[i], key_set, (size_t) key_set_len)) {
            for (int j = key_set_len; j < strlen(argv[i]) + 1; ++j)
                data[j - key_set_len] = argv[i][j];

            if ((tag.v2_tag_begin != -1 && isFrameNameExists((unsigned char *) data, getTagVersion(&tag))) ||
                (tag.v1_tag_begin != -1 && isFrameNameExists((unsigned char *) data, -1))) {
                if (i == argc - 1) {
                    printf("\nFew arguments for '%s', expected %s after %s\n", argv[i], key_value, key_set);
                } else if (tag.v2_tag_begin != -1 && !isTextFrameWriteable((unsigned char *) data, &tag)) {
                    printf("\nFrame '%s' is read-only\n", data);
                } else {
                    waiting_for_value = true;
                    strcpy((char *) set_frame_id, data);
                }
            } else {
                printf("\nFrame '%s' if not supported\n", data);
                printf("Call %s to view supported frames for this file\n", key_frames);
            }
        }
        else if (!strncmp(argv[i], key_help, (size_t) key_help_len)) {
            helpInfo();
        }

    }

    if (file_need_to_write) {
        if (!saveTagToBuffer(&tag,
                             old_v2tag_begin_pos, olg_v2tag_full_size,
                             old_v1tag_begin_pos,
                             &buffer, &buffer_size)) {
            printf("Saving tag error!\n");
            return 0;
        } else {
            char out_filepath[255];
            strncpy(out_filepath, filepath, strlen(filepath) - 4);
            out_filepath[strlen(filepath) - 4] = '\0';
            strcat(out_filepath, "-edited.mp3");

            FILE *fout = fopen(out_filepath, "wb");
            if (fout == NULL) {
                printf("\nWriting tag error");
            }
            fwrite(buffer, sizeof(unsigned char), (size_t) buffer_size, fout);
            fclose(fout);
        }
    }
    return 0;
}