#ifndef ID3V2_TAG_EDITOR_TAG_ID3V2_H
#define ID3V2_TAG_EDITOR_TAG_ID3V2_H

#include "tag_id3/tag_id3v2_header.h"
#include "tag_id3/tag_id3v2_frames.h"
#include "tag_id3/tag_id3v1.h"

#include <stdio.h>
#include <stdbool.h>
#include <malloc.h>

typedef struct {
    ID3v2Header header;
    unsigned char* frames_bytes;
    long tag_size;
    long frames_size;
    long frames_begin;
} ID3v2Tag;

typedef struct {
    ID3v1Tag v1_tag;
    long v1_tag_begin;
    ID3v2Tag v2_tag;
    long v2_tag_begin;
} ID3Tag;

// Returns:
// 1        - saving success
// 0        - saving failed
// other    - ID3v2 tag version not supported
int id3TagSave(const char *filepath, ID3Tag *tag, unsigned char **buffer, long *buffer_size);

bool saveTagToBuffer(ID3Tag *tag,
                     long v2_tag_begin_old, long v2_tag_size_old,
                     long v1_tag_begin_old,
                     unsigned char **buffer, long *buffer_size);

int getTagVersion(ID3Tag *tag);

bool printSupportedFrames(ID3Tag *tag, FILE *file_to_print);

int getTextFrameData(unsigned char *frame_id, ID3Tag *tag, unsigned char **out_data);

bool printTextFrame(unsigned char *frame_id, FILE* file_to_print, ID3Tag *tag, bool print_not_exist);

void printAllTextFramesData(FILE *file_to_print, ID3Tag *tag, bool print_not_exist);

int setTextFrameData(unsigned char frame_id[], ID3Tag *tag, unsigned char* in_data, long in_data_size, bool read_only);

bool isTextFrameWriteable(unsigned char frame_id[], ID3Tag *tag);

bool createID3v2Tag(ID3Tag *tag);

bool isFrameNameExists(const unsigned char *frame_id, int tag_version);
// TODO
void tagFree(ID3Tag* tag);


#endif //ID3V2_TAG_EDITOR_TAG_ID3V2_H
