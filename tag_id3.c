#include "tag_id3.h"

int id3TagSave(const char *filepath, ID3Tag *tag, unsigned char **buffer, long *buffer_size) {
    *buffer_size = 0;

    FILE* input_file = fopen(filepath, "rb");
    if (input_file == NULL)
        return 0;

    if (fseek(input_file, 0, SEEK_END))
        return 0;

    *buffer_size = ftell(input_file);
    *buffer = (unsigned char *) malloc(*buffer_size * sizeof(unsigned char));
    if (*buffer == NULL)
        return 0;

    fseek(input_file, 0, SEEK_SET);
    if (!fread(*buffer, sizeof(unsigned char), (size_t) *buffer_size, input_file)) {
        free(*buffer);
        *buffer = NULL;
        *buffer_size = 0;
        fclose(input_file);
        return 0;
    }
    fclose(input_file);

    if (!v2HeaderRead(*buffer, *buffer_size, &(tag->v2_tag.header), &(tag->v2_tag_begin), &(tag->v2_tag.tag_size))) {
        tag->v2_tag_begin = -1;
        tag->v2_tag.tag_size = 0;
        tag->v2_tag.frames_size = 0;
    }
    else {
        tag->v2_tag.frames_size = tag->v2_tag.tag_size;
        tag->v2_tag.frames_begin = tag->v2_tag_begin + 10;

        if (tag->v2_tag.header.extended_flag) {
            tag->v2_tag.frames_size -= 10;
            tag->v2_tag.frames_begin += 10;
        }
        if (tag->v2_tag.header.crc_flag) {
            tag->v2_tag.frames_size -= 4;
            tag->v2_tag.frames_begin += 4;
        }

        tag->v2_tag.frames_bytes = (unsigned char*) malloc(tag->v2_tag.frames_size * sizeof(unsigned char));
        for (long i = 0; i < tag->v2_tag.frames_size; i++)
            tag->v2_tag.frames_bytes[i] = *(*buffer + i + tag->v2_tag.frames_begin);
    }

    if (!v1TagRead(*buffer, *buffer_size, &(tag->v1_tag), &(tag->v1_tag_begin)))
        tag->v1_tag_begin = -1;
    return 1;
}

bool saveTagToBuffer(ID3Tag *tag,
                     long v2_tag_begin_old, long v2_tag_size_old,
                     long v1_tag_begin_old,
                     unsigned char **buffer, long *buffer_size) {
    const unsigned char FILLER = 0;
    // Erasing old data
    if (v1_tag_begin_old != -1 && tag->v1_tag_begin != -1) {
        if (!deleteFromBuffer(v1_tag_begin_old, 128, buffer, buffer_size))
            return false;
    }

    if (v2_tag_begin_old != -1 && tag->v2_tag_begin != -1) {
        if (!deleteFromBuffer(v2_tag_begin_old, v2_tag_size_old, buffer, buffer_size))
            return false;
    }
    if (tag->v2_tag_begin != -1) {
        long insert_size = tag->v2_tag.tag_size;
        if (tag->v2_tag.header.extended_flag) {
            insert_size += 10;
            if (tag->v2_tag.header.crc_flag)
                insert_size += 10;
        }

        if (!insertFillerToBuffer(FILLER, insert_size, 0, buffer, buffer_size))
            return false;

        int delim = 0;
        for (int i = 0; i < 10; ++i)
            *(*buffer + delim + i) = tag->v2_tag.header.main.bytes[i];
        delim += 10;

        if (tag->v2_tag.header.extended_flag) {
            for (int i = 0; i < 10; ++i)
                *(*buffer + delim + i) = tag->v2_tag.header.main.bytes[i];
            delim += 10;

            if (tag->v2_tag.header.crc_flag) {

                for (int i = 0; i < 10; ++i)
                    *(*buffer + delim + i) = tag->v2_tag.header.extended.crc[i];
                delim += 10;
            }
        }

        for (long i = 0; i < tag->v2_tag.frames_size; ++i)
            *(*buffer + delim + i) = tag->v2_tag.frames_bytes[i];
    }

    if (tag->v1_tag_begin != -1) {
        if (!insertFillerToBuffer(FILLER, 128, *buffer_size - 1, buffer, buffer_size))
            return false;

        for (int i = 0; i < 128; ++i)
            *(*buffer + *buffer_size - 128 + i) = tag->v1_tag.bytes[i];
    }
    return true;
}

int getTagVersion(ID3Tag *tag) {
    if (tag->v2_tag_begin != -1)
        return tag->v2_tag.header.main.fields.version;
    return -1;
}

bool printSupportedFrames(ID3Tag *tag, FILE *file_to_print) {
    if (tag->v2_tag_begin == -1) {
        char frame_ids[7][5] = {"TCON", "TRCK", "TDRC", "TPE1", "TPE2", "TALB", "TIT2"};
        for (int i = 0; i < 7; ++i)
            fprintf(file_to_print, "'%s'\n", frame_ids[i]);
        return 1;
    }

    if (getTagVersion(tag) != 3 && getTagVersion(tag) != 4)
        return 0;

    fprintf(file_to_print, "Supported ID3v2.%u frames:\n", getTagVersion(tag));
    printSupportedTextFrames(getTagVersion(tag), file_to_print);
    return 1;
}

int getTextFrameData(unsigned char *frame_id, ID3Tag *tag, unsigned char **out_data) {
    ID3TextFrame frame;
    int return_value = 0;
    if (tag->v2_tag_begin != -1 &&
            readTextFrame(frame_id, &frame, tag->v2_tag.frames_bytes, tag->v2_tag.frames_size)) {
        long out_size = 0;
        if (!frame.encoding) {
            *out_data = frame.data;
            return 2;
        }
        else if (convertUCS2toASCII(frame.data, frame.size, out_data, &out_size))
            return 2;
        else
            return_value = -2;
    }

    if (tag->v1_tag_begin != -1 && v1TagGet(frame_id, &(tag->v1_tag), out_data))
        return 1 + return_value;

    return return_value;
}

bool printTextFrame(unsigned char *frame_id, FILE* file_to_print, ID3Tag *tag, bool print_not_exist) {
    bool return_value = true;
    unsigned char *frame_data = NULL;

    switch (getTextFrameData(frame_id, tag, &frame_data)) {
        case 0:
            return_value = false;
            if (!print_not_exist)
                return false;
            fprintf(file_to_print, "'%s'", frame_id);
            fprintf(file_to_print, " - Frame not found :c\n");
            break;

        case 1:
            if (!print_not_exist && !strncmp((char *)frame_data, "[No info]", sizeof("[No info]")))
                return false;
            fprintf(file_to_print, "'%s'", frame_id);
            fprintf(file_to_print, " [data from v1 tag]:  ");
            if (!strcmp((char *) frame_data, ""))
                fprintf(file_to_print, "< clear frame >\n");
            else
                fprintf(file_to_print, "%s\n", frame_data);
            break;

        case 2:
            fprintf(file_to_print, "'%s'", frame_id);
            fprintf(file_to_print, " [data from v2 tag]:  ");
            if (!strcmp((char *) frame_data, ""))
                fprintf(file_to_print, "< clear frame >\n");
            else
                fprintf(file_to_print, "%s\n", frame_data);
            break;

        case -1:
            if (!print_not_exist && !strncmp((char *)frame_data, "[No info]", sizeof("[No info]"))) {
                fprintf(file_to_print, "'%s'", frame_id);
                fprintf(file_to_print, "--- WARNING! Encoding of data v2 tag frame '%s' is not "
                                       "supported ---\n", frame_id);
                return false;
            }
            fprintf(file_to_print, "'%s'", frame_id);
            fprintf(file_to_print, " [data from v1 tag]:  ");

            if (!strcmp((char *) frame_data, ""))
                fprintf(file_to_print, "< clear frame >\n");
            else
                fprintf(file_to_print, "%s\n", frame_data);

            fprintf(file_to_print, "--- WARNING! Encoding of data v2 tag frame '%s' is not "
                                   "supported ---\n", frame_id);
            break;
        case -2:
            fprintf(file_to_print, "'%s'", frame_id);
            fprintf(file_to_print, "--- WARNING! Encoding of data v2 tag frame '%s' is "
                                   "not supported ---\n", frame_id);
            break;

        default:
            break;
    }
    if (frame_data != NULL)
        free(frame_data);
    return return_value;
}

void printAllTextFramesData(FILE *file_to_print, ID3Tag *tag, bool print_not_exist) {
    if (tag->v2_tag_begin != -1) {
        if (tag->v2_tag.header.main.fields.version == 4) {
            for (int i = 0; i < V4_TEXT_FRAMES_COUNT; ++i)
                printTextFrame((unsigned char *) v4_text_frames_names[i], file_to_print, tag, print_not_exist);
        }
        else if (tag->v2_tag.header.main.fields.version == 3){
            for (int i = 0; i < V3_TEXT_FRAMES_COUNT; ++i)
                printTextFrame((unsigned char *) v3_text_frames_names[i], file_to_print, tag, print_not_exist);
        }
    }
    else {
        unsigned char frame_ids[7][5] = {"TALB", "TIT2", "TPE1", "TPE2", "TRCK", "TCON", "TDRC"};
        for (int i = 0; i < 7; ++i)
            printTextFrame((unsigned char *) frame_ids[i], file_to_print, tag, print_not_exist);
    }
}

int setTextFrameData(unsigned char frame_id[], ID3Tag *tag, unsigned char* in_data, long in_data_size, bool read_only) {
    int success_counter = 0;
    if (!isTextFrameNameExists(frame_id, getTagVersion(tag)))
        return -1;

    ID3TextFrame frame;
    createFrame(frame_id, in_data, in_data_size, read_only, &frame);

    long temp = tag->v2_tag.frames_size;
    if (tag->v2_tag_begin != -1 &&
            writeTextFrame(frame_id, &frame, &(tag->v2_tag.frames_bytes), &(tag->v2_tag.frames_size))) {
        tag->v2_tag.tag_size -= temp;
        tag->v2_tag.tag_size += tag->v2_tag.frames_size;
        v2HeaderUpdateSize(&(tag->v2_tag.header), tag->v2_tag.tag_size);

        success_counter += 2;
    }
    if (tag->v1_tag_begin != -1 && v1TagSet(frame_id, &(tag->v1_tag), in_data, in_data_size))
        success_counter++;

    return success_counter;
}

bool isTextFrameWriteable(unsigned char frame_id[], ID3Tag *tag) {
    ID3TextFrame frame;
    readTextFrame(frame_id, &frame, tag->v2_tag.frames_bytes, tag->v2_tag.frames_size);
    bool flag = !frame.read_only_flag;
    free(frame.data);
    return flag;
}

bool createID3v2Tag(ID3Tag *tag) {
    if (tag->v1_tag_begin == -1 || tag->v2_tag_begin != -1)
        return false;
    unsigned char frame_ids[7][5] = {"TCON", "TRCK", "TDRC", "TPE1", "TPE2", "TALB", "TIT2"};

    tag->v2_tag.header.extended_flag = 0;

    tag->v2_tag.header.main.fields.marker[0] = (unsigned char)'I';
    tag->v2_tag.header.main.fields.marker[1] = (unsigned char)'D';
    tag->v2_tag.header.main.fields.marker[2] = (unsigned char)'3';
    tag->v2_tag.header.main.fields.version = 4;
    tag->v2_tag.header.main.fields.sub_version = 0;
    tag->v2_tag.header.main.fields.flags = 0;
    for (int j = 0; j < 4; j++)
        tag->v2_tag.header.main.fields.pieced_size[j] = 0;

    tag->v2_tag.frames_size = 0;
    tag->v2_tag.frames_bytes = NULL;

    tag->v2_tag_begin = 0;
    tag->v2_tag.tag_size = 0;
    tag->v2_tag.frames_begin = 10;

    unsigned char *data = NULL;
    long data_size = 0;
    for (int i = 0; i < 7; ++i) {
        v1TagGet(frame_ids[i], &(tag->v1_tag), &data);
        data_size = (long) strlen((char *) data);

        if (!strcmp((char *) data, ""))
            continue;

        bool read_only_flag = false;
        if (!strncmp("TPE1", (char *)frame_ids[i], 4))
            read_only_flag = true;

        ID3TextFrame frame;
        createFrame(frame_ids[i], data, data_size, read_only_flag, &frame);

        long temp = tag->v2_tag.frames_size;
        if (!writeTextFrame(frame_ids[i], &frame, &(tag->v2_tag.frames_bytes), &(tag->v2_tag.frames_size))) {
            tag->v2_tag_begin = -1;
            free(tag->v2_tag.frames_bytes);
            free(data);
            return false;
        }

        tag->v2_tag.tag_size -= temp;
        tag->v2_tag.tag_size += tag->v2_tag.frames_size;
        v2HeaderUpdateSize(&(tag->v2_tag.header), tag->v2_tag.tag_size);
    }
    return true;
}

bool isFrameNameExists(const unsigned char *frame_id, int tag_version) {
    if (tag_version == -1) {
        char frame_ids[7][5] = {"TCON", "TRCK", "TDRC", "TPE1", "TPE2", "TALB", "TIT2"};
        for (int i = 0; i < V3_TEXT_FRAMES_COUNT; ++i) {
            if (!strncmp((char *) frame_id, (char *) frame_ids[i], 4))
                return true;
        }
        return false;
    } else {
        return isTextFrameNameExists(frame_id, tag_version);
    }
    return false;
}

// TODO
void tagFree(ID3Tag* tag) {
    free(tag->v2_tag.frames_bytes);
    tag->v2_tag.frames_size = 0;
}