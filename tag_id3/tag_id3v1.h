#ifndef ID3V2_TAG_EDITOR_TAG_ID3V1_H
#define ID3V2_TAG_EDITOR_TAG_ID3V1_H

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <stdlib.h>

typedef union {
    struct {
        unsigned char marker[3];
        unsigned char title[30];
        unsigned char artist[30];
        unsigned char album[30];
        unsigned char year[4];
        // comment[] also include trackNo, if comment[28] == 0
        unsigned char comment[30];
        unsigned char genre;
    } fields;

    unsigned char bytes[128];
} ID3v1Tag;

extern const unsigned char genres[80][32];

bool v1TagRead(const unsigned char* buffer,
              long buffer_size,
              ID3v1Tag* tag,
              long* tag_begin_pos);

bool v1TagGet(const unsigned char frame_id[], const ID3v1Tag* tag, unsigned char **out_data);

bool v1TagSet(const unsigned char frame_id[], ID3v1Tag* tag, unsigned char* in_data, int in_data_size);

#endif //ID3V2_TAG_EDITOR_TAG_ID3V1_H
