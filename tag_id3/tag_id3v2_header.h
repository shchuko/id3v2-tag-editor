#ifndef ID3V2_TAG_EDITOR_HEADER_READER_H
#define ID3V2_TAG_EDITOR_HEADER_READER_H

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

typedef struct {
    union {
        struct {
            unsigned char marker[3];
            unsigned char version;
            unsigned char sub_version;
            unsigned char flags;
            unsigned char pieced_size[4];
        } fields;
        unsigned char bytes[10];
    } main;

    union {
        struct {
            unsigned char size[4];
            unsigned char flags[2];
            unsigned char padding_size[4];
        };
        struct {
            unsigned char static_data[10];
            unsigned char crc[10];
        };
    } extended;

    bool extended_flag;
    bool crc_flag;
} ID3v2Header;

// Returns:
// 1        - reading success
// 0        - reading failed
// other    - tag version
bool v2HeaderRead(unsigned char* buffer,
                  long buffer_size,
                  ID3v2Header* headers,
                  long* tag_begin_pos,
                  long* tag_size);

void v2HeaderUpdateSize(ID3v2Header *headers, long new_size);

#endif //ID3V2_TAG_EDITOR_HEADER_READER_H
