#include "tag_id3v2_header.h"

bool v2HeaderRead(unsigned char* buffer,
                  long buffer_size,
                  ID3v2Header* headers,
                  long* tag_begin_pos,
                  long* tag_size) {
    headers->extended_flag = false;
    headers->crc_flag = false;

    for (int i = 0; i < buffer_size - 2; ++i) {
        // Searching ID3v2 tag beginning
        if (!(strncmp((char *) (buffer + i), "ID3", 3)) && (buffer[i + 5] & 0b00001111) == 0) {
            *tag_begin_pos = i;
            for (int j = 0; j < 10; ++j) {
                headers->main.bytes[j] = buffer[j + i];
            }

            // Calculating and saving the length of Tag (in bytes)
            // NOT includes Header length!
            *tag_size = 0;
            for (int j = 0; j < 4; ++j)
                *tag_size += (headers->main.fields.pieced_size[3 - j] & 0x7F) << 7 * j;

            // Saving extended header, if it exists
            headers->extended_flag = (bool) ((headers->main.fields.flags >> 6) & 1);
            if (headers->extended_flag) {
                for (int j = 0; j < 10; ++j)
                    headers->extended.static_data[j] = buffer[j + i + 10];

                // Saving CRC data, if it exists
                headers->crc_flag = (bool) ((headers->extended.flags[0] >> 7) & 1);
                if (headers->crc_flag) {
                    for (int j = 0; j < 4; ++j)
                        headers->extended.crc[j] = buffer[j + i + 20];
                }
            }
            return true;
        }
    }
    return false;
}

void v2HeaderUpdateSize(ID3v2Header *headers, long new_size) {
    for (int i = 0; i < 4; ++i)
        headers->main.fields.pieced_size[i] = (unsigned char) ((new_size >> (7 * (3 - i))) & 0x7F);
}