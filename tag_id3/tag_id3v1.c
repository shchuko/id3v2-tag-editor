#include "tag_id3v1.h"

bool v1TagRead(const unsigned char* buffer,
                  const long buffer_size,
                  ID3v1Tag* tag,
                  long* tag_begin_pos) {
    if (!(strncmp((char *) (buffer + buffer_size - 128), "TAG", 3))) {
        *tag_begin_pos = buffer_size - 128;
        for (int j = 0; j < 128; j++) {
            tag->bytes[j] = buffer[j + buffer_size - 128];
        }
        return true;
    }
    return false;
}

bool v1TagGet(const unsigned char frame_id[], const ID3v1Tag* tag, unsigned char **out_data) {
    if (!strcmp((char*) frame_id, "TALB")) {
        if (*out_data != NULL) free(*out_data);
        *out_data = (unsigned char *) malloc(31 * sizeof(unsigned char));

        for (int i = 0; i < 30; ++i) {
            *(*out_data + i) = tag->fields.album[i];
        }
        *(*out_data + 30) = '\0';
        return true;
    }

    if (!strcmp((char*) frame_id, "TIT2")) {
        if (*out_data != NULL) free(*out_data);
        *out_data = (unsigned char*) malloc(31 * sizeof(unsigned char));
        for (int i = 0; i < 30; ++i) {
            *(*out_data + i) = tag->fields.title[i];
        }
        *(*out_data + 30) = '\0';
        return true;
    }

    if (!strcmp((char*) frame_id, "TPE2") || !strcmp((char*) frame_id, "TPE1")) {
        if (*out_data != NULL) free(*out_data);
        *out_data = (unsigned char*) malloc(31 * sizeof(unsigned char));
        for (int i = 0; i < 30; ++i) {
            *(*out_data + i) = tag->fields.artist[i];
        }
        *(*out_data + 30) = '\0';
        return true;
    }

    if (!strcmp((char*) frame_id, "TYER") || !strcmp((char*) frame_id, "TDRC")) {
        if (*out_data != NULL) free(*out_data);
        *out_data = (unsigned char*) malloc(5 * sizeof(unsigned char));
        for (int i = 0; i < 4; ++i) {
            *(*out_data + i) = tag->fields.year[i];
        }
        *(*out_data + 4) = '\0';
        return true;
    }

    if (!strcmp((char*) frame_id, "TRCK")) {
        if (*out_data != NULL) free(*out_data);
        if (!tag->fields.comment[28]) {
            int counter = 0;
            unsigned char temp = tag->fields.comment[29];
            while(temp > 0) {
                counter++;
                temp /= 10;
            }
            *out_data = (unsigned char*) malloc(counter * sizeof(unsigned char) + 1);
            itoa(tag->fields.comment[29], (char*) *out_data, 10);
            *(*out_data + counter) = '\0';
        }
        else {
            *out_data = (unsigned char*) malloc((strlen("[No info]") + 1) * sizeof(unsigned char));
            unsigned char temp[] = "[No info]";
            for (int i = 0; i < strlen("[No info]") + 1; i++)
                *(*out_data + i) = temp[i];
            return false;
        }
        return true;
    }

    if (!strcmp((char*) frame_id, "COMM")) {
        if (*out_data != NULL) free(*out_data);
        if (!tag->fields.comment[28]) {
            *out_data = (unsigned char *) malloc(28 * sizeof(unsigned char));
            for (int i = 0; i < 27; ++i) {
                *(*out_data + i) = tag->fields.comment[i];
            }
            *(*out_data + 27) = '\0';
        } else {
            *out_data = (unsigned char *) malloc(31 * sizeof(unsigned char));
            for (int i = 0; i < 30; ++i) {
                *(*out_data + i) = tag->fields.comment[i];
            }
            *(*out_data + 30) = '\0';
        }

        bool flag = 0;
        for (int i = 0; i < strlen((char*) *out_data); i++)
            flag |= (unsigned char) ' ' != *(*out_data + i);

        if (!flag) {
            realloc(*out_data, (strlen("[No info]") + 1) * sizeof(unsigned char));
            unsigned char temp[] = "[No info]";
            for (int i = 0; i < strlen("[No info]") + 1; i++)
                *(*out_data + i) = temp[i];
            return false;
        }
    }

   if (!strcmp((char*) frame_id, "TCON")) {
       if (*out_data != NULL) free(*out_data);
       if (tag->fields.genre > 79) {
           *out_data = (unsigned  char*) malloc((strlen("[No info]") + 1) * sizeof(unsigned char));
           unsigned char temp[] = "[No info]";
           for (int i = 0; i < strlen("[No info]") + 1; i++)
               *(*out_data + i) = temp[i];
           return false;
       } else {
           *out_data = (unsigned char*) malloc(strlen((char*) genres[tag->fields.genre]) * sizeof(unsigned char));
           for (int i = 0; i < strlen((char*) genres[tag->fields.genre]) + 1; i++)
               *(*out_data + i) = genres[tag->fields.genre][i];
       }
       return true;
   }

    return 0;
}

bool v1TagSet(const unsigned char frame_id[], ID3v1Tag* tag, unsigned char* in_data, int in_data_size) {
    if (!strcmp((char*) frame_id, "TALB")) {
        if (in_data_size <= 30) {
            for (int i = 0; i < in_data_size; i++)
                tag->fields.album[i] = in_data[i];
            for (int i = in_data_size; i < 30; i++)
                tag->fields.album[i] = ' ';
        }
        else {
            for (int i = 0; i < 30; i++)
                tag->fields.album[i] = in_data[i];
        }
        return true;
    }

    if (!strcmp((char*) frame_id, "TIT2")) {
        if (in_data_size <= 30) {
            for (int i = 0; i < in_data_size; i++)
                tag->fields.title[i] = in_data[i];
            for (int i = in_data_size; i < 30; i++)
                tag->fields.title[i] = ' ';
        } else {
            for (int i = 0; i < 30; i++)
                tag->fields.title[i] = in_data[i];
        }
        return true;
    }

    if (!strcmp((char *) frame_id, "TPE2") || !strcmp((char *) frame_id, "TPE1")) {
        if (in_data_size <= 30) {
            for (int i = 0; i < in_data_size; i++)
                tag->fields.artist[i] = in_data[i];
            for (int i = in_data_size; i < 30; i++)
                tag->fields.artist[i] = ' ';
        } else {
            for (int i = 0; i < 30; i++)
                tag->fields.artist[i] = in_data[i];
        }
        return true;
    }

    if (!strcmp((char*) frame_id, "TYER") || !strcmp((char*) frame_id, "TDRC")) {
        for (int i = 0; i < 4; i++)
            tag->fields.year[i] = in_data[i];
        return true;
    }

    // FIXME
    if (!strcmp((char*) frame_id, "TRCK")) {
        int delim_symbol_pos = in_data_size - 1;
        for (int i = 0; i < in_data_size; ++i) {
            if (in_data[i] == '/') {
                delim_symbol_pos = i;
                break;
            }
        }
        char temp[3] = "   ";
        strncpy(temp, (char*) in_data, (size_t) delim_symbol_pos + 1);
        tag->fields.comment[28] = 0;
        if (atoi(temp) >= 0 && atoi(temp) <= 255)
            tag->fields.comment[29] = (unsigned char) atoi(temp);
        else
            tag->fields.comment[28] = tag->fields.comment[29] = ' ';

        return true;
    }

    if (!strcmp((char*) frame_id, "COMM")) {
        int border = 28;
        bool end_flag = false;
        if (tag->fields.comment[28]) { border = 30; }
        if (border < in_data_size) {
            border = in_data_size;
            end_flag = true;
        }

        for (int i = 0; i < border; i++)
            tag->fields.comment[i] = in_data[i];

        if (end_flag)
            for (int i = border; i < 28; i++)
                tag->fields.comment[i] = ' ';
        else
            for (int i = border; i < 30; i++)
                tag->fields.comment[i] = ' ';
        return true;
    }

    if (!strcmp((char*) frame_id, "TCON")) {
        int i = -1;
        while (in_data[++i] != '(' && i < in_data_size) { }
        int j = i;
        while (in_data[++j] != ')' && j < in_data_size) { }
        if (i < in_data_size && j - i > 1) {
            char temp[3];
            strncpy(temp, (char*)(in_data + i + 1), (size_t) j - i - 1);
            if (atoi(temp) < 255 && atoi(temp) >= 0)
                tag->fields.genre = (unsigned char) atoi(temp);
        } else {
            tag->fields.genre = 255;
        }
        return true;
    }

    printf("%s\n", frame_id);
    return false;
}

const unsigned char genres[80][32] = {
        "Blues",
        "Classic Rock",
        "Country",
        "Dance",
        "Disco",
        "Funk",
        "Grunge",
        "Hip-Hop",
        "Jazz",
        "Metal",
        "New Age",
        "Oldies",
        "Other",
        "Pop",
        "R&B",
        "Rap",
        "Reggae",
        "Rock",
        "Techno",
        "Industrial",
        "Alternative",
        "Ska",
        "Death Metal",
        "Pranks",
        "Soundtrack",
        "Euro-Techno",
        "Ambient",
        "Trip-Hop",
        "Vocal",
        "Jazz+Funk",
        "Fusion",
        "Trance",
        "Classical",
        "Instrumental",
        "Acid",
        "House",
        "Game",
        "Sound Clip",
        "Gospel",
        "Noise",
        "AlternRock",
        "Bass",
        "Soul",
        "Punk",
        "Space",
        "Meditative",
        "Instrumental Pop",
        "Instrumental Rock",
        "Ethnic",
        "Gothic",
        "Darkwave",
        "Techno-Industrial",
        "Electronic",
        "Pop-Folk",
        "Eurodance",
        "Dream",
        "Southern Rock",
        "Comedy",
        "Cult",
        "Gangsta",
        "Top 40",
        "Christian Rap",
        "Pop/Funk",
        "Jungle",
        "Native American",
        "Cabaret",
        "New Wave",
        "Psychadelic",
        "Rave",
        "Showtunes",
        "Trailer",
        "Lo-Fi",
        "Tribal",
        "Acid Punk",
        "Acid Jazz",
        "Polka",
        "Retro",
        "Musical",
        "Rock & Roll",
        "Hard Rock"
};