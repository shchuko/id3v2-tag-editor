#include "tag_id3v2_frames.h"

const unsigned char v3_text_frames_names[V3_TEXT_FRAMES_COUNT][5] = {
        "TALB",
        "TBPM",
        "TCOM",
        "TCON",
        "TCOP",
        "TDAT",
        "TDLY",
        "TENC",
        "TEXT",
        "TFLT",
        "TIME",
        "TIT1",
        "TIT2",
        "TIT3",
        "TKEY",
        "TLAN",
        "TLEN",
        "TMED",
        "TOAL",
        "TOFN",
        "TOLY",
        "TOPE",
        "TORY",
        "TOWN",
        "TPE1",
        "TPE2",
        "TPE3",
        "TPE4",
        "TPOS",
        "TPUB",
        "TRCK",
        "TRDA",
        "TRSN",
        "TRSO",
        "TSIZ",
        "TSRC",
        "TSSE",
        "TYER"
};

const unsigned char v3_text_frames_info[V3_TEXT_FRAMES_COUNT][127] = {
        "Album/Movie/Show title",
        "BPM (beats per minute)",
        "Composer",
        "Content type",
        "Copyright message",
        "Date",
        "Playlist delay",
        "Encoded by",
        "Lyricist/Text writer",
        "File type",
        "Time",
        "Content group description",
        "Title/songname/content description",
        "Subtitle/Description refinement",
        "Initial key",
        "Language(s)",
        "Length",
        "Media type",
        "Original album/movie/show title",
        "Original filename",
        "Original lyricist(s)/text writer(s)",
        "Original artist(s)/performer(s)",
        "Original release year",
        "File owner/licensee",
        "Lead performer(s)/Soloist(s)",
        "Band/orchestra/accompaniment",
        "Conductor/performer refinement",
        "Interpreted, remixed, or otherwise modified by",
        "Part of a set",
        "Publisher",
        "Track number/Position in set",
        "Recording dates",
        "Internet radio station name",
        "Internet radio station owner",
        "Size",
        "ISRC (international standard recording code)",
        "Software/Hardware and settings used for encoding",
        "Year"
};

const unsigned char v4_text_frames_names[V4_TEXT_FRAMES_COUNT][5] = {
        "TALB",
        "TBPM",
        "TCOM",
        "TCON",
        "TCOP",
        "TDEN",
        "TDLY",
        "TDOR",
        "TDRC",
        "TDRL",
        "TDTG",
        "TENC",
        "TEXT",
        "TFLT",
        "TIPL",
        "TIT1",
        "TIT2",
        "TIT3",
        "TKEY",
        "TLAN",
        "TLEN",
        "TMCL",
        "TMED",
        "TMOO",
        "TOAL",
        "TOFN",
        "TOLY",
        "TOPE",
        "TOWN",
        "TPE1",
        "TPE2",
        "TPE3",
        "TPE4",
        "TPOS",
        "TPRO",
        "TPUB",
        "TRCK",
        "TRSN",
        "TRSO",
        "TSOA",
        "TSOP",
        "TSOT",
        "TSRC",
        "TSSE",
        "TSST"
};

const unsigned char v4_text_frames_info[V4_TEXT_FRAMES_COUNT][127] = {
        "Album/Movie/Show title",
        "BPM (beats per minute)",
        "Composer",
        "Content type",
        "Copyright message",
        "Encoding time",
        "Playlist delay",
        "Original release time",
        "Recording time",
        "Release time",
        "Tagging time",
        "Encoded by",
        "Lyricist/Text writer",
        "File type",
        "Involved people list",
        "Content group description",
        "Title/songname/content description",
        "Subtitle/Description refinement",
        "Initial key",
        "Language(s)",
        "Length",
        "Musician credits list",
        "Media type",
        "Mood",
        "Original album/movie/show title",
        "Original filename",
        "Original lyricist(s)/text writer(s)",
        "Original artist(s)/performer(s)",
        "File owner/licensee",
        "Lead performer(s)/Soloist(s)",
        "Band/orchestra/accompaniment",
        "Conductor/performer refinement",
        "Interpreted, remixed, or otherwise modified by",
        "Part of a set",
        "Produced notice",
        "Publisher",
        "Track number/Position in set",
        "Internet radio station name",
        "Internet radio station owner",
        "Album sort order",
        "Performer sort order",
        "Title sort order",
        "ISRC (international standard recording code)",
        "Software/Hardware and settings used for encoding",
        "Set subtitle"
};

bool insertToBuffer(const unsigned char *data, long data_size,
                    long split_pos,
                    unsigned char **buffer, long *buffer_size) {
    unsigned char *temp_buffer = NULL;
    temp_buffer = (unsigned char*) malloc(((size_t) *buffer_size + data_size) * sizeof(unsigned char));
    if (temp_buffer == NULL)
        return false;

    for (long i = 0; i < split_pos; ++i)
        temp_buffer[i] = *(*buffer + i);
    for (long i = 0; i < data_size; ++i)
        temp_buffer[i + split_pos] = data[i];
    for (long i = split_pos; i < *buffer_size; ++i)
        temp_buffer[i  + data_size] = *(*buffer + i);

    free(*buffer);
    *buffer_size += data_size;
    *buffer = temp_buffer;
    return true;
}

bool insertFillerToBuffer(unsigned char filler, long size,
                            long split_pos,
                            unsigned char **buffer, long *buffer_size) {
    unsigned char *temp_buffer = NULL;
    temp_buffer = (unsigned char*) malloc(((size_t) *buffer_size + size) * sizeof(unsigned char));
    if (temp_buffer == NULL)
        return false;

    for (long i = 0; i < split_pos; ++i)
        temp_buffer[i] = *(*buffer + i);
    for (long i = 0; i < size; ++i)
        temp_buffer[i + split_pos] = filler;
    for (long i = split_pos; i < *buffer_size; ++i)
        temp_buffer[i  + size] = *(*buffer + i);

    free(*buffer);
    *buffer_size += size;
    *buffer = temp_buffer;
    return true;
}

bool deleteFromBuffer(long start_pos, long delete_size, unsigned char **buffer, long *buffer_size) {
    unsigned char *temp_buffer = NULL;
    temp_buffer = (unsigned char *) malloc(((size_t) *buffer_size - delete_size) * sizeof(unsigned char));
    if (temp_buffer == NULL)
        return false;

    for (long i = 0; i < start_pos; ++i)
        temp_buffer[i] = *(*buffer + i);
    for (long i = start_pos + delete_size; i < *buffer_size; ++i)
        temp_buffer[i - delete_size] = *(*buffer + i);

    free(*buffer);
    *buffer_size -= delete_size;
    *buffer = temp_buffer;
    return true;
}

void printSupportedTextFrames(int tag_version, FILE *file_to_print) {
    fprintf(file_to_print, " - Text frames: \n");

    if (tag_version == 4)
        for (int i = 0 ; i < V4_TEXT_FRAMES_COUNT ; ++i) {
            fprintf(file_to_print, "\t%s - ", v4_text_frames_names[i]);
            fprintf(file_to_print, "%s\n", v4_text_frames_info[i]);
        }
    else if (tag_version == 3)
        for (int i = 0; i < V3_TEXT_FRAMES_COUNT; ++i) {
            fprintf(file_to_print, "\t%s - ", v3_text_frames_names[i]);
            fprintf(file_to_print, "%s\n", v3_text_frames_info[i]);
        }
}

bool readTextFrame(const unsigned char *frame_id, ID3TextFrame *frame,
                   const unsigned char *buffer, long buffer_size) {
    for (int i = 0; i < buffer_size - 3; ++i) {
        unsigned char read_id[4];
        strncpy((char *) read_id, (char *) (buffer + i), 4);

        // Searching for frame beginning
        if (!(strncmp((char *) read_id, (char*) frame_id, 4))) {
            frame->begin_position = i;
            for (int j = 0; j < 10; ++j)
                frame->header.bytes[j] = buffer[j + i];
            frame->read_only_flag = (bool) ((frame->header.fields.flags[0] >> 5) & 1);

            // Saving size
            frame->size = 0;
            for (int j = 3; j >= 0; --j)
                frame->size += (((long) frame->header.fields.size[j]) << (3 - j) * 8);

            frame->encoding = buffer[i + 10];
            frame->data = (unsigned char*) malloc(frame->size * sizeof(unsigned char));

            // Saving frame data executing "encoding" byte
            for (long j = 0; j < frame->size - 1; ++j)
                frame->data[j] = buffer[i + j + 11];
            frame->data[frame->size - 1] = '\0';
            return true;
        }
    }
    return false;
}


bool convertUCS2toASCII(const unsigned char *in_string, long in_size,
                        unsigned char **out_string, long *out_size) {
    if (in_string[0] == 0xFF && in_string[1] == 0xFE) {
        *out_size = (long) ceil((in_size - 2) / 2.0);
        *out_string = (unsigned char*) malloc(*out_size * sizeof(unsigned char));
        for (int i = 2; i < in_size; ++i) {
            if (!(i % 2))
                *(*out_string + (i - 2) / 2) = in_string[i];
            else if (in_string[i] != 0) {
                free(*out_string);
                *out_string = NULL;
                *out_size = 0;
                return false;
            }
        }
        return true;
    } else if (in_string[0] == 0xFE && in_string[1] == 0xFF) {
        *out_size = (long) ceil((in_size - 2) / 2.0);
        *out_string = (unsigned char*) malloc(*out_size * sizeof(unsigned char));

        for (int i = 2; i < in_size; ++i) {
            if (i % 2)
                *(*out_string + (i - 2) / 2) = in_string[i];
            else if (in_string[i] != 0) {
                free(*out_string);
                *out_string = NULL;
                *out_size = 0;
                return false;
            }
        }
        return true;
    } else {
        free(*out_string);
        *out_string = NULL;
        *out_size = 0;
        return false;
    }
}

void convertASCIItoUCS2(const unsigned char *in_string, long in_size,
                        unsigned char **out_string, long *out_size) {
    *out_size = in_size * 2 + 2;
    *out_string = (unsigned char*) malloc(*out_size * sizeof(unsigned char));
    *(*out_string + 0) = 0xFF;
    *(*out_string + 1) = 0xFE;

    for (long i = 0; i < *out_size - 2; ++i) {
        if (i % 2)
            *(*out_string + i + 2) = 0;
        else
            *(*out_string + i + 2) = in_string[i / 2];
    }
}

bool isTextFrameNameExists(const unsigned char *frame_id, int tag_version) {
    if (tag_version == 4) {
        for (int i = 0; i < V4_TEXT_FRAMES_COUNT; ++i) {
            if (!strncmp((char *) frame_id, (char *) v4_text_frames_names[i], 4))
                return true;
        }
        return false;
    } else if (tag_version == 3) {
        for (int i = 0; i < V3_TEXT_FRAMES_COUNT; ++i) {
            if (!strncmp((char *) frame_id, (char *) v3_text_frames_names[i], 4))
                return true;
        }
        return false;
    }
    return false;
}

bool writeTextFrame(const unsigned char *frame_id, ID3TextFrame *frame, unsigned char **buffer, long *buffer_size) {
    ID3TextFrame readed_frame;
    bool need_to_resize = false;

    // If the same frame found
    if (readTextFrame(frame_id, &readed_frame, *buffer, *buffer_size)) {
        // And it is not read-only, we need to resize space in buffer
        if (readed_frame.read_only_flag)
            return false;
        else
            need_to_resize = true;
    }

    // Resizing
    if (need_to_resize) {
        if (frame->size > readed_frame.size) {
            unsigned char null_data = 0;
            if (!insertFillerToBuffer(null_data,
                                frame->size - readed_frame.size,
                                readed_frame.begin_position + readed_frame.size + 10,
                                buffer, buffer_size))
                return false;

        } else if (frame->size < readed_frame.size) {
            if (!deleteFromBuffer(readed_frame.begin_position,
                                  readed_frame.size - frame->size,
                                  buffer, buffer_size))
                return false;
        }

        // Saving frame after resizing
        for (int i = 0; i < 10; ++i)
            *(*buffer + readed_frame.begin_position + i) = frame->header.bytes[i];
        *(*buffer + readed_frame.begin_position + 10) = frame->encoding;
        for (long i = 0; i < frame->size - 1; ++i)
            *(*buffer + readed_frame.begin_position + 11 + i) = frame->data[i];
        return true;
    }

    // If there was not the same frame, saving data to the beginning of frames space
    unsigned char null_data = 0;
    if (!insertToBuffer(&null_data,
                        frame->size + 10,
                        0,
                        buffer, buffer_size))
        return false;

    for (int i = 0; i < 10; ++i)
        *(*buffer + i) = frame->header.bytes[i];
    *(*buffer + 10) = frame->encoding;
    for (long i = 0; i < frame->size - 1; ++i)
        *(*buffer + 11 + i) = frame->data[i];
    return true;
}

bool deleteTextFrame(const unsigned char *frame_id, unsigned char **buffer, long *buffer_size) {
    ID3TextFrame readed_frame;

    // If the same frame found
    if (readTextFrame(frame_id, &readed_frame, *buffer, *buffer_size)) {
        // And it is not read-only, we need can delete it
        if (readed_frame.read_only_flag)
            return false;
        if (!deleteFromBuffer(readed_frame.begin_position, readed_frame.size + 10, buffer, buffer_size))
            return false;
        return true;
    }
    return false;
}

void createFrame(const unsigned char *frame_id,
                            const unsigned char *data,
                            long data_size,
                            bool read_only_flag,
                            ID3TextFrame *frame) {

    frame->begin_position = -1;
    frame->header.fields.flags[0] = 0;
    frame->header.fields.flags[1] = 0;

    frame->read_only_flag = read_only_flag;
    frame->header.fields.flags[0] |= ((read_only_flag << 5) & (1 << 5));
    frame->encoding = 0;

    frame->size = data_size + 1;
    frame->data = (unsigned char*) malloc(frame->size * sizeof(unsigned char));
    if (frame->data == NULL)
        return;

    for (int i = 0; i < 4; ++i)
        frame->header.fields.id[i] = frame_id[i];
    for (int i = 0; i < data_size; ++i)
        frame->data[i] = data[i];


    for (int i = 0; i < 4; ++i)
        frame->header.fields.size[i] = (unsigned char) ((frame->size >> (8 * (3 - i))) & 0xFF);
}