//
// Created by Nikita on 27.10.2018.
//

#ifndef ID3V2_TAG_EDITOR_FRAME_READER_H
#define ID3V2_TAG_EDITOR_FRAME_READER_H

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <malloc.h>
#include <math.h>

#define V4_TEXT_FRAMES_COUNT 45
#define V3_TEXT_FRAMES_COUNT 38

extern const unsigned char v4_text_frames_names[V4_TEXT_FRAMES_COUNT][5];
extern const unsigned char v4_text_frames_info[V4_TEXT_FRAMES_COUNT][127];

extern const unsigned char v3_text_frames_names[V3_TEXT_FRAMES_COUNT][5];
extern const unsigned char v3_text_frames_info[V3_TEXT_FRAMES_COUNT][127];

typedef struct {
    union {
        struct {
            unsigned char id[4];
            unsigned char size[4];
            unsigned char flags[2];
        } fields;
        unsigned char bytes[10];
    } header;
    unsigned char encoding;
    unsigned char *data;

    long size;
    long begin_position;
    bool read_only_flag;
} ID3TextFrame;

bool insertToBuffer(const unsigned char *data, long data_size,
                    long split_pos,
                    unsigned char **buffer, long *buffer_size);
bool insertFillerToBuffer(unsigned char filler, long size,
                          long split_pos,
                          unsigned char **buffer, long *buffer_size);

bool deleteFromBuffer(long start_pos, long delete_size, unsigned char **buffer, long *buffer_size);

bool readTextFrame(const unsigned char *frame_id, ID3TextFrame *frame,
                   const unsigned char *buffer, long buffer_size);

bool writeTextFrame(const unsigned char *frame_id, ID3TextFrame *frame,
                    unsigned char **buffer, long *buffer_size);

bool deleteTextFrame(const unsigned char *frame_id, unsigned char **buffer, long *buffer_size);

void createFrame(const unsigned char *frame_id,
                 const unsigned char *data,
                 long data_size,
                 bool read_only_flag,
                 ID3TextFrame *frame);

void printSupportedTextFrames(int tag_version, FILE *file_to_print);

bool isTextFrameNameExists(const unsigned char *frame_id, int tag_version);

bool convertUCS2toASCII(const unsigned char *in_string, long in_size,
                        unsigned char **out_string, long *out_size);

void convertASCIItoUCS2(const unsigned char *in_string, long in_size,
                        unsigned char **out_string, long *out_size);

#endif //ID3V2_TAG_EDITOR_FRAME_READER_H
